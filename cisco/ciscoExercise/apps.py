from django.apps import AppConfig


class CiscoexerciseConfig(AppConfig):
    name = 'ciscoExercise'
