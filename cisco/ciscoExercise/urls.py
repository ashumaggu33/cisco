from django.urls import path
from ciscoExercise import  views

urlpatterns = [
    path('record-data/',  views.RecordView.as_view(), name='record'),
    path('create/',  views.CreateRouter.as_view(), name='create'),
    path('update/',  views.UpdateRecord.as_view(), name='update'),
    path('delete/',  views.DeleteRecord.as_view(), name='delete'),
]