from django.shortcuts import render
from django.views.generic import ListView
from .models import RouterRecord
from django.views.generic import View
from django.http import JsonResponse, HttpResponse


class RecordView(View):
    def get(self, request):
        model = RouterRecord.objects.filter(active=True).values()
        print(model)
        template_name = 'data/record.html'
        data = {
            'RouterRecord': model
        }
        return render(request, template_name, data)


class CreateRouter(View):
    def  get(self, request):
        sapId = request.GET.get('sapId', None)
        hostName = request.GET.get('hostName', None)
        loopBack = request.GET.get('loopBack', None)
        macAddress = request.GET.get('macAddress', None)

        obj = RouterRecord.objects.create(
            sapId = sapId,
            hostName = hostName,
            loopBack = loopBack, macAddress=macAddress
        )

        router = {'id':obj.id,'sapId':obj.sapId,'hostName':obj.hostName,'loopBack':obj.loopBack, 'macAddress':obj.macAddress}

        data = {
            'RouterRecord': router
        }
        return JsonResponse(data)


class UpdateRecord(View):
    def  get(self, request):
        id1 = request.GET.get('id', None)
        sapId = request.GET.get('sapId', None)
        hostName = request.GET.get('hostName', None)
        loopBack = request.GET.get('loopBack', None)
        macAddress = request.GET.get('macAddress', None)

        obj = RouterRecord.objects.get(id=id1)
        obj.sapId = sapId
        obj.hostName = hostName
        obj.loopBack = loopBack
        obj.macAddress = macAddress
        obj.save()

        router = {'id':obj.id,'sapId':obj.sapId,'hostName':obj.hostName,'loopBack':obj.loopBack, 'macAddress':obj.macAddress}

        data = {
            'RouterRecord': router
        }
        return JsonResponse(data)


class DeleteRecord(View):
    def  get(self, request):
        id1 = request.GET.get('id', None)
        obj = RouterRecord.objects.get(id=id1)
        obj.active = False
        obj.save()
        data = {
            'deleted': True
        }
        return JsonResponse(data)