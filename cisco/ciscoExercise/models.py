from django.db import models

class RouterRecord(models.Model):
    sapId = models.CharField(max_length=18, blank=True)
    hostName = models.CharField(max_length=14, blank=True)
    loopBack = models.CharField(max_length=100, blank=True)
    macAddress = models.CharField(max_length=17, blank=True)
    active = models.BooleanField(blank=True, default=True)
