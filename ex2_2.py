from ftplib import FTP
from shutil import make_archive
from io import BytesIO
from ftplib import FTP_TLS

# Connect to the FTP server by specifying the host name

ftp = FTP(host="dlptest.com")

print(ftp.getwelcome())
ftpResponse = ftp.login(user="username", passwd="password")
print(ftpResponse)
ftpResponse = ftp.mkd("directory_to_be_created")
print(ftpResponse)
shutil.make_archive('zip_folder_name', 'zip', ftpResponse)
ZipFile.write(filename)
with FTP_TLS(ftp_host) as ftp:
    ftp.login(user=ftp_user, passwd=ftp_pass)
    ftp.prot_p()
    with download_file = BytesIO():
        ftp.retrbinary('RETR ' + fp, download_file.write)
        download_file.seek(0)

ftpResponse = ftp.dir()
print(ftpResponse)