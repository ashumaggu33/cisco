#!/bin/bash
for I in 0 1 2 3 4 5; do
    check=$(uptime | tr -d ',.' | awk '{print $10}')
    if [ "$check" -gt 5 ]; then
        /usr/bin/systemctl restart httpd.service
    fi
    sleep 10
done



* * * * * auto_restart.sh > logs.log 2>&1




#ex 2_4_1
df -h

#ex 2_4_2
top/htop

#ex 2_4_3
renice -p <PID> <PRIORITY>



#ex 2_5
CREATE VIEW [Router Records] AS
SELECT sapId, hostName
FROM RouterRecords
WHERE macAddress = "" and active=1;



#ex 2_6
INSERT INTO RouterRecords
  ( sapId, hostName, macAddress, loopBack )
VALUES
  ('123', 'sadsad', '123.12.10.36', ''),
  ('125', 'sadsad', '123.12.10.36', ''),
  ('127', 'sadsad', '123.12.10.36', ''),
  ('120', 'sadsad', '123.12.10.36', ''),
  ('126', 'sadsad', '123.12.10.36', ''),
  ('122', 'sadsad', '123.12.10.36', ''),
  ('129', 'sadsad', '123.12.10.36', ''),
  ('130', 'sadsad', '123.12.10.36', ''),
  ('133', 'sadsad', '123.12.10.36', ''),
  ('132', 'sadsad', '123.12.10.36', '');
