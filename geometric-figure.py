import numpy as np
import cv2
import imutils
my_img = np.zeros((400, 400, 3), dtype = "uint8")
# creating a rectangle
color = (255, 0, 0)
thickness = 2
cv2.rectangle(my_img, (90, 80), (160, 120), color, thickness)
# my_img = imutils.rotate(my_img, 30)
# creating circle
cv2.circle(my_img, (110, 110), 60, color, thickness)
pts = np.array([[25, 70], [25, 160],
                [110, 200], [200, 160],
                [200, 70], [110, 20]],
               np.int32)

pts = pts.reshape((-1, 1, 2))
isClosed = True
my_img = cv2.polylines(my_img, [pts], isClosed, color, thickness)
cv2.imshow('Window', my_img)
# allows us to see image
# until closed forcefully
cv2.waitKey(0)
cv2.destroyAllWindows()