from django.apps import AppConfig


class CiscotaskConfig(AppConfig):
    name = 'ciscoTask'
